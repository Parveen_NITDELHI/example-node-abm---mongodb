'use strict'

const mongoose = require ('mongoose');
const Schema = mongoose.Schema;
const bcrypt = require('bcrypt');

const UserSchema = new Schema({
    email: {type: String, unique: true, lowercase: true},
    password: String,
    name: String,
    singUpDate: {type: Date, default: Date.now()},
});

//hash password before saving to database
UserSchema.pre('save', function(next)  {
  let user = this;
  bcrypt.hash(user.password, 10, function(error, hash) {
    if (error) {
      return next(error);
    } else {
      user.password = hash;
      next();
    }
  });
});

//compare Password
UserSchema.methods.comparePassword = function (candidatePassword, callback) {
  
  bcrypt.compare(candidatePassword, this.password, (err, isMatch) => {
    callback(err, isMatch)
  });
}

module.exports = mongoose.model('user',UserSchema);