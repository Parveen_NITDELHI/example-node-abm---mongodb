//use mongoose
const mongoose = require ('mongoose');
const Schema = mongoose.Schema;

//product model
const UserSchema = new Schema({
    name: String,
    category: String,
    price: Number,
    user: String
});

//export module
module.exports = mongoose.model('product',UserSchema);