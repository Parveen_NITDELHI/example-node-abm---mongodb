'use strict'


const User = require('../Models/users');
const service = require('../Services/index');

function singUp(req,res) {
    
    User.findOne({ email: req.body.email }, (err, userDuplicate) => {
        if (err) res.status(200).send({ msg: `Error al crear el usuario: ${err}`, status: false });
        if (userDuplicate) res.status(200).send({ msg: 'El usuario ya existe', status: false })
        let user = new User({
            email: req.body.email,
            password: req.body.password,
            name: req.body.name
        });
        user.save((error) => {
            if (error) res.status(200).send({ msg: `Error al crear el usuario: ${error}`, status: false });
            res.status(200).send({ msg: `usuario: ${req.body.email} creado correctamente`, status: true, token: service.createToken(user) });
        });
    });
}


function singIn(req,res) {

    User.findOne({email: req.body.email}, (err, user) => {
        if (err) return res.status(500).send({ msg: `Error al ingresar: ${err}` })
        if (!user) return res.status(404).send({ msg: `no existe el usuario: ${req.body.email}` })
        
        return user.comparePassword(req.body.password, (err, isMatch) => {
          if (err) return res.status(500).send({ msg: `Error al ingresar: ${err}` })
          if (!isMatch) return res.status(404).send({ msg: `Error de contraseña: ${req.body.email}` })
    
            req.user = user;
            return res.status(200).send({ msg: 'Te has logueado correctamente', token: service.createToken(user) })            
        });
    }).select('_id email password');
}

//traer data de usuarios
function userData(req,res) {
    var id =  service.decodeToken(req.body.token);
    id.then(function(result){
        var _id = result; // Now you can use res everywhere
        User.findOne({_id:_id }, (err, user) => {
            if (err) return res.status(500).send({ msg: `Error al ingresar: ${err}` })
            res.status(200).send(user);
        }).select('displayName + email  + name')  //only displayName and email   
    })
}
//Update User
function upUser (req, res) {
    var id = req.body._id;

    User.findById(id, function (err, user) {
        if (err) return res.status(500).send({ message: `Error al realizar la petición: ${err}` });        
        if (!user) return res.status(404).send({ message: 'No existe el usuario'});

        user.dcrypPass = req.body.password;
        user.password = req.body.password;
        user.displayName = req.body.displayName;
        user.active = req.body.active;
        user.maxImg = req.body.maxImg;

        user.save(function () {
            res.status(200).send({ message: 'Atualización exitosa!'});
        });
    });
}

//Delete a user
function rmUser (req,res) {
    const id = req.body.id;
    User.deleteOne({_id:id},function (error, promo) {
        if (error) res.status(200).send({msg: 'El usuario no ha podido ser eliminado.',status: false});
        res.status(200).send({msg:'El usuario ha podido ser eliminado con exito.', status: true});  
    });
}

//mostrar todos los usuarios
function getUsers (req,res) {
    User.find({}, (err,users) =>{
        if(err) return res.status(500).send({message: `Error al realizar la petición: ${err}`});  
        if(!users) return res.status(404).send({message: 'No existen usuarios'});
        
        users.forEach(u => {            
           u.token = service.createToken(u);
           //console.log('mosotrar token '+ u.displayName );      
        });        
        res.status(200).send({users});
    })   
}

module.exports = {
    singUp,
    singIn,
    userData,
    upUser,
    rmUser,
    getUsers,
};