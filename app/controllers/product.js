'use strict'

const Product = require('../models/product');

//create a new Product
function createProduct(req,res) {
    let product = new Product({
        name: req.body.name,
        category: req.body.category,
        price: req.body.price,
        user: req.body.user
    });

    product.save((error) => {
        if (error) res.status(200).send({ msg: `Error al crear el producto: ${error}`, status: false });
        res.status(200).send({ msg: `producto: ${req.body.name} creado correctamente`, status: true});
        });
    }

//Update Product
function updateProduct (req, res) {
    var id = req.body.id;

    Product.findById(id, function (err, product) {
        if (err) return res.status(500).send({ msg: `Error al realizar la petición: ${err}`, status: false});        
        if (!product) return res.status(404).send({ msg: 'No existe producto', status: false});

        product.name= req.body.name,
        product.category = req.body.category;
        product.price = req.body.price;
        product.user = req.body.user;

        product.save(function () {
            res.status(200).send({msg: 'Atualización exitosa!',status: true});
        });
    });
}
//Delete a Product
function deleteProduct (req,res) {
    const id = req.body.id;
    Product.deleteOne({_id:id},function (error, product) {
        if (error) res.status(200).send({msg: 'No ha podido ser borrado.',status: false});
        res.status(200).send({msg:'El producto ser eliminado con exito.', status: true});  
    });
}

//show all products
function getProducts (req,res) {
    Product.find({}, (err,products) =>{
        if(err) return res.status(500).send({message: `Error al realizar la petición: ${err}`});  
        if(!products) return res.status(200).send({message: 'No existen productos'});
        
        res.status(200).send({products});
    })   
}

module.exports = {
    createProduct,
    deleteProduct,
    updateProduct,
    getProducts
};