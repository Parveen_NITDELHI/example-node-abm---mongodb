'use strict'
//problema al instalar esta funcionaba sin el metodo encode
//const jwt = require('jsonwebtoken');

//Importación de modulos
const moment = require('moment');
const config = require('../../config/config');
const jwt = require('jwt-simple');

function createToken(user) {
    const payload = {
        sub: user._id,
        iat: moment().unix(),
        exp: moment().add(100,'years').unix()
    }
    return jwt.encode(payload ,config.SECRET_TOKEN);
}

function decodeToken(token) {
    const decode = new Promise((resolve,reject) =>{
        try {
            const payload = jwt.decode(token, config.SECRET_TOKEN);

            if (payload.exp <= moment().unix()) {
                reject({ 
                    status: 401,
                    mesagge: 'El token ha expirado' 
                })
            }
            resolve(payload.sub);
        } catch (error) {
            reject({
                status: 500,
                message: 'Token invalido'
            });
        }

    })
    return decode;
}

module.exports = {
    createToken,
    decodeToken
};