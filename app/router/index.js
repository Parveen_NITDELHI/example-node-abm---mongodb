'use strict'

//import node modules
const express = require('express');

//Import controllers
const userController= require('../controllers/user');
const productController = require('../controllers/product');

//router 
const router = express.Router();
const baseUrl = require('../../config/config').baseUrl;

//test autentificacion
const auth = require('../Middelware/auth');

// VIEWS
router.get('/', function (req, res) {
    res.sendFile(baseUrl+'views/login/autentification.html');
});
// home page
router.get('/index', function (req, res) {
    res.sendFile(baseUrl+'views/login/autentification.html');
});

//register Page
router.get('/registration', function (req, res) {
    res.sendFile(baseUrl+'views/login/newUser.html');
});




//user Page
router.get('/start',auth.isAuth, function (req,res) {     
    res.sendFile(baseUrl+'views/admin/start.html');
})
router.get('/abm',auth.isAuth, function (req,res) {     
    res.sendFile(baseUrl+'views/admin/abm.html');
})

router.get('/table',auth.isAuth, function (req,res) {     
    res.sendFile(baseUrl+'views/admin/table.html');
})


//METHODS
//redireccionador
router.post('/authorization',auth.isAuth)

//method controller login
router.post('/singIn', userController.singIn);
router.post('/singUp', userController.singUp);
router.post('/getUserData',userController.userData);
//Users
router.post('/upUser',userController.upUser);
router.post('/rmUser',userController.rmUser);

//Products
router.post('/createProduct',productController.createProduct);
router.post('/getProducts',productController.getProducts);
router.post('/updateProduct',productController.updateProduct);
router.post('/deleteProduct',productController.deleteProduct);
module.exports = router;