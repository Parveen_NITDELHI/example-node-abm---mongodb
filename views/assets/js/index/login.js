
window.onload = function () {
    init();
}
//initializate events
function init() {
    const login = document.querySelector('#form-index');

    login.addEventListener('submit',event =>{
        event.preventDefault();
        event.stopPropagation();

        var email =  document.querySelector('#signin-email');
        var password =  document.querySelector('#signin-password'); 
        var data = {email:email.value,password: password.value};

        fetch('/exampleNode/singIn',{ 
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }  
        })
        .then(res => res.json())
        .then(function (res) {
            var message = res.msg;
            var token = res.token;

            if(typeof token !== typeof undefined && typeof token !== typeof null){
                Events.saveInLocalStorage({key:'token',value:token});
                Events.admin(token,'/exampleNode/start');
            } else {
                Events.addErrorMessage(message);
            }
        });
    })
}




