
window.onload = function () {
    init();
}

function init() {
    const singUp = document.querySelector('#form-index');

    singUp.addEventListener('submit',event =>{
        //stop refres page
        event.preventDefault();
        event.stopPropagation();

        //inputs values 
        const email =  document.querySelector('#signUp-email').value;
        const name = document.querySelector('#signUp-name').value;
        const password =  document.querySelector('#signUp-password').value;
        const password2 =  document.querySelector('#signUp-password2').value;

        //disable button
        singUp.disabled = true

        //
        if (password !== password2) {
            Events.addErrorMessage("Las contraseñas no son iguales");
        } else {
            var data = {email:email, password: password, name: name};
            //ajax event
            fetch('/exampleNode/singUp',{ 
                method: 'POST',
                body: JSON.stringify(data),
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }  
            })
            .then(res => res.json())
            .then(function (res) {
                singUp.disabled = false
                
                var message = res.msg;
                var status = res.status;

                if(status === false){
                    Events.addErrorMessage(message);
                } else {
                    Events.addSuccessMessage(message);
                    Events.addSuccessMessage("Redirigiendo a la pag de inicio...");
                    //redirecction index page
                    setTimeout(function(){ location.href = "/exampleNode"; }, 3000);
                }
            });
        }

    })
}




