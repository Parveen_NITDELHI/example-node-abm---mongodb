'use strict'

//import node modules
const mongoose = require('mongoose');
const app = require('./app/app');
const configApp = require('./config/config');
const configDB = require('./config/database');

//connect mongo db 
mongoose.connect(configDB.url, function (err,res) {
    
    if (err) {
        console.log('Error al conectar la base de datos');
    } else {
        console.log('Connección a la base de datos establecida');
        app.listen(configApp.port, () => {
            console.log(`corriendo puerto: ${configApp.port}`);
        });
    }
    
})