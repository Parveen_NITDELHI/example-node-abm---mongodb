# Ejemplo Node ABM - MongoDB

La idea de este ejemplo es mostrar las habilidades y conocmientos adquiridos en JS tanto del lado del cliente, como del lado del servidor conjuntamente con la base de datos mongo.
Lo compone creación/login de usuario basado en tokens, abm de productos fácilmente adaptable para cualquier otro fin, sistemas de stock, admin en general etc.

## Contenido
### Ejemplo de ABM:

* Altas Bajas y Modificaciones con MongoDB
* Cuenta con una lista de Productos que pueden agregarse, borrarse y eliminarse

### Ejemplo de un tabla cargada con un archivo externo:
* La tabla se carga con un archivo en formato clave: valor (JSON)

### Instalación

*  Tener activo y en uso Mondo DB.
* Desacargar los archivos, y utilizar el npm de Node.js para descargar los paquetes.
* Ejecutar a traves de nodemoon con el comando "npm start".

### Sobre las técnologias utilizadas
La aplicación esta realizada con diversas técnologias:

* Servidor: Node.js, entre sus paquetes mas importantes (express,mongoose,bcrypt,jwt-simple)
* Librerias js: jquery(necesaria por bootstrap), iziToast(alertas), tingle (modal)
* Estilos y diseño: Krolofil (plantilla de uso gratiuto), bootstrap.

### Capturas

Registro de Nuevo Usuario:

![](https://gitlab.com/atalivar77/example-node-abm---mongodb/raw/master/capturas/1.png)

Login de Usuario:

![](https://gitlab.com/atalivar77/example-node-abm---mongodb/raw/master/capturas/2.png)

Datatable:

![](https://gitlab.com/atalivar77/example-node-abm---mongodb/raw/master/capturas/3.png)

Alta Baja y Modificaciones de Productos :

![](https://gitlab.com/atalivar77/example-node-abm---mongodb/raw/master/capturas/4.png)
